
$(document).ready(function() {
  let current = 0;
  let velocidad = 1;

  prepareCameras(function( camaras ){
    let random = new Date().getTime();    
    let c = camaras;
    for(var i in c){
      $('<div class="carousel-item"><img class="d-block w-100" src="'+c[i].url+'?'+random+'"></div>').appendTo('.carousel-inner');
    }    
    $('#myCarousel').carousel();  
  });

});